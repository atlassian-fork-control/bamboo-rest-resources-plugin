package com.atlassian.bamboo.plugin.plantemplates.rest;


import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.bamboo.deployments.versions.DeploymentVersion;
import com.atlassian.bamboo.deployments.versions.history.issues.DeploymentVersionLinkedJiraIssue;
import com.atlassian.bamboo.deployments.versions.service.DeploymentVersionLinkedJiraIssuesService;
import com.atlassian.bamboo.deployments.versions.service.DeploymentVersionService;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.List;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/")
public class DeploymentResource
{
    private final DeploymentVersionService deploymentVersionService;
    private final DeploymentVersionLinkedJiraIssuesService deploymentVersionLinkedJiraIssuesService;
    private final DeploymentResultService deploymentResultService;
    private DeploymentProjectService deploymentProjectService;

    private static final Logger logger = LoggerFactory.getLogger(DeploymentResource.class);

    public DeploymentResource(DeploymentVersionService deploymentVersionService,
                              DeploymentVersionLinkedJiraIssuesService deploymentVersionLinkedJiraIssuesService,
                              DeploymentResultService deploymentResultService,
                              DeploymentProjectService deploymentProjectService)
    {
        this.deploymentVersionService = deploymentVersionService;
        this.deploymentVersionLinkedJiraIssuesService = deploymentVersionLinkedJiraIssuesService;
        this.deploymentResultService = deploymentResultService;
        this.deploymentProjectService = deploymentProjectService;
    }


    @WebSudoRequired
    @Path("/deployment/{deploymentProjectName}/release/{releaseName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlanTemPlateExecutionStatus(@PathParam("deploymentProjectName") final String deploymentProjectName, @PathParam("releaseName") String releaseName)
    {
        List<DeploymentProject> deploymentProjects = deploymentProjectService.getAllDeploymentProjects();
        Optional<DeploymentProject> deploymentProject = Iterables.tryFind(deploymentProjects, new Predicate<DeploymentProject>() {
            @Override
            public boolean apply(DeploymentProject deploymentProject) {
                return deploymentProject.getName().equals(deploymentProjectName);
            }
        });

        if(deploymentProject.isPresent())
        {
            long deploymentProjectIdNum = deploymentProject.get().getId();
            DeploymentVersion deploymentVersion = deploymentVersionService.getDeploymentVersionByName(releaseName, deploymentProjectIdNum);
            logger.info("Ged deployment verison by name: {} {} {}", releaseName, deploymentProjectIdNum, deploymentVersion);

            List<DeploymentVersionLinkedJiraIssue> issues = deploymentVersionLinkedJiraIssuesService.getJiraIssuesForDeploymentVersion(deploymentVersion.getId());
            logger.info("Get issues: deploymentVersion {}", issues);
            Iterable<String> issueKeys = Iterables.transform(issues, new Function<DeploymentVersionLinkedJiraIssue, String>() {
                @Override
                public String apply(DeploymentVersionLinkedJiraIssue input) {
                    return input.getIssueKey();
                }
            });

            return Response.ok(issueKeys).build();
        }
        else
        {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid deployment project name: " + deploymentProjectName).build();
        }

    }

}
